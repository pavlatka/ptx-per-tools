<?php

$container['google_client'] = function ($c) {
    $client = new Google_Client();
    $client->setApplicationName('Trello Life Planning');
    $client->setScopes(Google_Service_Calendar::CALENDAR);
    $client->setAuthConfig(__DIR__ . '/../../../../data/google/credentials.json');
    $client->setRedirectUri('https://ptx-tools.herokuapp.com/google/auth_callback');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
    $client->setAccessToken(file_get_contents(__DIR__ . '/../../../../data/google/token.json'));

    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
    }

    return $client;
};

$container['google_calendar_service'] = function ($c) {
    return new Google_Service_Calendar(
        $c['google_client']
    );
};
