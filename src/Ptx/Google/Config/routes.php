<?php

use \Slim\Http\Request;
use \Slim\Http\Response;

$app->group('/google', function () use ($app) {
    $app->get('/auth_callback', function (Request $request, Response $response, $args) {
        return $response->withJSON([]);
    });

    $app->group('/calendars', function () use ($app) {
        $app->get('', function (Request $request, Response $response, $args) {
            $calendarsInfo   = [];
            $calendarService = $this->google_calendar_service;

            $calendars = $calendarService->calendarList->listCalendarList();
            foreach ($calendars as $calendar) {
                $calendarsInfo[] = [
                    'id'      => $calendar->id,
                    'summary' => $calendar->summary
                ];
            }

            return $response->withJSON($calendarsInfo);
        });

        $app->get('/events', function (Request $request, Response $response, $args) {
            $tid             = 'dBPpy5aAu36cjt2FUyCdr';
            $calendarId      = 'snn1mm71psk4v9dicrm82t9hbg@group.calendar.google.com';
            $calendarService = $this->google_calendar_service;

            $optParams = [
                'maxResults' => 2,
                'q'          => '[TID:' . $tid . ']'
            ];

            $events = $calendarService->events->listEvents($calendarId, $optParams);

            echo '<pre>';
            var_dump($events);
            echo '</pre>';
            exit;
        });

        $app->post('/events', function (Request $request, Response $response, $args) {
            $tid             = 'dBPpy5aAu36cjt2FUyCdr';
            $calendarId      = 'snn1mm71psk4v9dicrm82t9hbg@group.calendar.google.com';
            $calendarService = $this->google_calendar_service;


            $eventData = [
                'summary'     => 'Test Event',
                'description' => 'This is my test event <br /> [TID:' . $tid . ']',
                'start'       => [
                    'dateTime' => '2018-12-21T09:00:00',
                    'timeZone' => 'Europe/Berlin'
                ],
                'end'       => [
                    'dateTime' => '2018-12-21T09:30:00',
                    'timeZone' => 'Europe/Berlin'
                ],
                'reminders' => [
                ]
            ];

            $event   = new Google_Service_Calendar_Event($eventData);
            $eventId = 'dtd4qdprokdnh8caogmbdogimk';

            $calendarService->events->update($calendarId, $eventId, $event);
        });
    });
});
