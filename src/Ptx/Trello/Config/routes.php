<?php

use \Slim\Http\Request;
use \Slim\Http\Response;

$app->group('/trello', function () use ($app) {
    $app->map(['HEAD'], '/card', function (Request $request, Response $response, $args) {
        return $response;
    });

    $app->get('/settings', function (Request $request, Response $response, $args) {
        $service = $this->trello_settings_service;

        return $response->withJson($service->getSettings());
    });

    $app->post('/forecast', function (Request $request, Response $response, $args) {
        try {
            $service = $this->trello_forecast_service;
            $service->run();

            return $response->withJson(['ok' => true]);
        } catch (\Exception $e) {
            return $response->withJson([
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ], 500);
        }
    });

    $app->post('/archiver', function (Request $request, Response $response, $args) {
        try {
            $cardListId = '5bebb36f92de50665237901d';
            $service = $this->trello_archiver_service;
            $service->run($cardListId);

            return $response->withJson(['ok' => true]);
        } catch (\Exception $e) {
            return $response->withJson([
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ], 500);
        }
    });

    $app->post('/planner', function (Request $request, Response $response, $args) {
        try {
            $service = $this->trello_planner_service;
            $service->run();

            return $response->withJson(['ok' => true]);
        } catch (\Exception $e) {
            return $response->withJson([
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ], 500);
        }
    });

    $app->group('/finance', function () use ($app) {
        $app->put('/planner/{year:\d+}/{month:\d+}', function (Request $request, Response $response, $args) {
            try {
                $financePlannerService = $this->trello_finance_planner_service;
                $financePlannerService->plan($args['year'], $args['month']);

                return $response->withJson(['ok' => true]);
            } catch (\Exception $e) {
                return $response->withJson([
                    'error' => $e->getMessage(),
                    'code'  => $e->getCode()
                ], 500);
            }
        });
        $app->put('/stats', function (Request $request, Response $response, $args) {
            try {
                $financeStatsService = $this->trello_finance_stats_service;
                $financeStatsService->updateStats();

                return $response->withJson(['ok' => true]);
            } catch (\Exception $e) {
                return $response->withJson([
                    'error' => $e->getMessage(),
                    'code'  => $e->getCode()
                ], 500);
            }
        });
    });

    $app->post('/card', function (Request $request, Response $response, $args) {
        try {
            $payload = json_decode($request->getBody(), true);
            error_log(json_encode($payload));

            $cardService = $this->trello_card_service;
            $cardService->manageCard($payload);

            $sortListService = $this->trello_sort_list_service;
            $sortListService->sortList($payload);

            //            $calendarService = $this->trello_google_calendar_service;
            //            $calendarService->setInCalendar($payload);

            $financeService = $this->trello_finance_service;
            $financeService->manageCard($payload);

            return $response->withJson(['ok' => true]);
        } catch (\Exception $e) {
            return $response->withJson([
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ], 500);
        }
    });

    $app->get('/prepare-day', function (Request $request, Response $response, $args) {
        $columns = [
            'done'     => '5bebb36f92de50665237901d',
            '2days'    => '5b8a1367b4438d1eb932326f',
            '5days'    => '5abce421f45eb0220819d653',
            '14days'   => '5abce4244c83575c282a02b4',
            'planning' => '5b2b9ce156f919d433cea793'
        ];

        $cards = [
            '2days'  => '5b8a13726c845069b0fc30fc',
            '5days'  => '5b837ee3378f551989153534',
            '14days' => '5b837f1d7bd4f38f31f004ae',
        ];

        try {
            $service  = $this->trello_prepare_day_service;
            $service->prepareDay($columns, $cards);

            return $response->withJson(['ok' => true]);
        } catch (\Exception $e) {
            return $response->withJson([
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ], 500);
        }
    });
});
