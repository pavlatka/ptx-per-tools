<?php

$container['trello_client'] = function ($c) {
    $apiKey       = getenv('TRELLO_API_KEY') ?? '';
    $accessToken  = getenv('TRELLO_ACCESS_TOKEN') ?? '';
    $trelloClient = new \Ptx\Trello\Extra\TrelloClient();
    $trelloClient->authenticate($apiKey, $accessToken, \Trello\Client::AUTH_URL_CLIENT_ID);

    return $trelloClient;
};

$container['trello_finance_planner_dao'] = function () {
    return new \Ptx\Trello\DAO\FinancePlannerDAO;
};

$container['trello_planner_dao'] = function () {
    return new \Ptx\Trello\DAO\PlannerDAO;
};

$container['trello_forecast_dao'] = function () {
    return new \Ptx\Trello\DAO\ForecastDAO;
};

$container['trello_prepare_day_service'] = function ($c) {
    return new \Ptx\Trello\Service\PrepareDay(
        $c['trello_client']
    );
};

$container['trello_archiver_service'] = function ($c) {
    return new \Ptx\Trello\Service\Archiver(
        $c['trello_client']
    );
};


$container['trello_card_service'] = function ($c) {
    $doneListId = '5bebb36f92de50665237901d';

    return new \Ptx\Trello\Service\Card(
        $c['trello_client'],
        $doneListId
    );
};

$container['trello_sort_list_service'] = function ($c) {
    return new \Ptx\Trello\Service\SortList(
        $c['trello_client']
    );
};

$container['trello_forecast_service'] = function ($c) {
    return new \Ptx\Trello\Service\Forecast(
        $c['trello_client'],
        $c['trello_forecast_dao']
    );
};

$container['trello_settings_service'] = function ($c) {
    return new \Ptx\Trello\Service\Settings(
        $c['trello_client']
    );
};

$container['trello_finance_service'] = function ($c) {
    return new \Ptx\Trello\Service\Finance(
        $c['trello_client']
    );
};

$container['trello_finance_stats_service'] = function ($c) {
    return new \Ptx\Trello\Service\FinanceStats(
        $c['trello_client'],
        '5c3eed1cda3a2b56d26e3e71'
    );
};

$container['trello_finance_planner_service'] = function ($c) {
    return new \Ptx\Trello\Service\FinancePlanner(
        $c['trello_client'],
        $c['trello_finance_planner_dao'],
        '5c3eed1cda3a2b56d26e3e71'
    );
};

$container['trello_planner_service'] = function ($c) {
    $settings = [
        'column' => '5b2b9ce156f919d433cea793',
        'labels' => [
            'GE' => '5b2b9d02fc8d0f498d2fde58',
            'HE' => '5abce3ec841642c2a82073f0',
            'LA' => '5abce3ec841642c2a82073f2',
            'LI' => '5abce3ec841642c2a82073f9',
            'TH' => '5abce3ec841642c2a82073f6',
            'TO' => '5abce3ec841642c2a82073fa'
        ]
    ];

    return new \Ptx\Trello\Service\Planner(
        $c['trello_client'],
        $c['trello_planner_dao'],
        $settings
    );
};

$container['trello_google_calendar_service'] = function ($c) {
    $calendarId = 'snn1mm71psk4v9dicrm82t9hbg@group.calendar.google.com';

    return new \Ptx\Trello\Service\GoogleCalendar(
        $calendarId,
        $c['trello_client'],
        $c['google_calendar_service']
    );
};
