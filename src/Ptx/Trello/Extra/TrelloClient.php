<?php
declare(strict_types=1);

namespace Ptx\Trello\Extra;

use \Trello\Client;

class TrelloClient extends Client
{
    public function api($name)
    {
        if ($name === 'card' || $name === 'cards') {
            return new TrelloCard($this);
        }

        return parent::api($name);
    }
}
