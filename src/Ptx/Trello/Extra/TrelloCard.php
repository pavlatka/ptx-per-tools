<?php
declare(strict_types=1);

namespace Ptx\Trello\Extra;

use Trello\Api\Card;

class TrelloCard extends Card
{
    public function setCardLabel($id, $labelId)
    {
        return $this->post($this->getPath().'/'.rawurlencode($id).'/idLabels', array('value' => $labelId));
    }
}
