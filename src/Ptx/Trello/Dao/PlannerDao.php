<?php
declare(strict_types=1);

namespace Ptx\Trello\DAO;

class PlannerDAO
{
    public function getData() : array
    {
        $data = [];

        $jsons = $this->getJsonData();
        foreach ($jsons as $json) {
            $data = array_merge($data, json_decode($json, true));
        }

        return $data;
    }

    private function getJsonData() : array
    {
        $folder = realpath(__DIR__ . '/../../../../data/trello/planner');
        $files  = glob($folder . '/*.json');

        $jsons = [];
        foreach ($files as $file) {
            $jsons[] = file_get_contents($file);
        }

        return $jsons;
    }
}
