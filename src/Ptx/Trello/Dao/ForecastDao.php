<?php
declare(strict_types=1);

namespace Ptx\Trello\DAO;

use SimpleXMLElement;

class ForecastDAO
{
    const BASE_URL = 'https://www.yr.no/place/';

    public function getForecast(string $location) : array
    {
        $locationUrl = $this->getUrlForLocation($location);
        $pureForecast = file_get_contents($locationUrl);
        $xmlForecast  = new SimpleXMLElement($pureForecast);

        $forecast = [];
        foreach ($xmlForecast->forecast->tabular->time as $tab) {
          $today       = date('Y-m-d');
          $controlDate = date('Y-m-d', strtotime($tab['from']->__toString()));
          if ($controlDate == $today) {
            return [
              [
                'temp'          => (int) $tab->temperature['value'],
                'wind'          => $tab->windSpeed['name']->__toString(),
                'wind_mps'      => (int) $tab->windSpeed['mps'],
                'precipitation' => (int) $tab->precipitation['value'],
                'forecast'      => $tab->symbol['name']->__toString()
              ]
            ];
          }
        }

        return [];
    }

    private function getUrlForLocation(string $location) : string
    {
        return self::BASE_URL . $location . '/forecast.xml';
    }
}
