<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

class FinanceStats
{
    const LABELS = [
        '5c3eed1ca3a82f48723fc5aa' => 'PAYMENT',
        '5c3eeda77c329d3db0dfd377' => 'PAID',
        '5c3eed1ca3a82f48723fc5a8' => 'INCOME',
        '5c3efebc5aa29f85aeaaf2e4' => 'HOUSE',
        '5c3eff29d3199665a254aaf3' => 'CAR',
        '5c41b63a22d7754383c6595e' => 'BVG',
        '5c41b6647c7ef33de1ac589c' => 'POCKET',
        '5c3efeca3f375256e76b6e98' => 'CLUB',
        '5c3eff004a269c85bfcedeee' => 'OTHER',
        '5c3efee76d0e4c5a656f85bf' => 'SCHOOL',
    ];

    const MONEY_LABELS = ['PAID'];

    const GROUP_CARD_IDS = [
        'INCOME'  => '5c41c342a4c81d8663a33fc9',
        'PAYMENT' => '5c41c33f6d418f76bf02f36b',
        'HOUSE'   => '5c41651e059bdc148ac83358',
        'CLUB'    => '5c4165322d957665103dd956',
        'CAR'     => '5c41656f74b663442391f4cd',
        'OTHER'   => '5c41652969ebcc51132b9f72',
        'SCHOOL'  => '5c41652c2c17414bb6944ce6',
        'BVG'     => '5c41b70dd1326e1dcd6b6d77',
        'POCKET'  => '5c41b70bdb925e14d93e8986',
    ];

    private $trelloClient;
    private $boardId;

    public function __construct(\Trello\Client $trelloClient, string $boardId)
    {
        $this->boardId      = $boardId;
        $this->trelloClient = $trelloClient;
    }

    public function updateStats() : void
    {
        $balances = [];
        $cards    = $this->trelloClient->api('board')->cards()->filter($this->boardId, 'open');

        foreach ($cards as $card) {
            if (!$this->canProcess($card['name'])) {
                continue;
            }

            $cardLabels = $this->getCardLabels($card['idLabels']);
            $cardAmount = $this->parseAmountFromCardName($card['name']);
            $cardGroups = array_diff($cardLabels, self::MONEY_LABELS);

            foreach ($cardGroups as $cardGroup) {
                if (!isset($balances[$cardGroup])) {
                    $balances[$cardGroup] = ['total' => 0, 'paid' => 0];
                }

                if (in_array('PAYMENT', $cardLabels) || in_array('INCOME', $cardLabels)) {
                    $balances[$cardGroup]['total'] += $cardAmount;
                }

                if (in_array('PAID', $cardLabels)) {
                    $balances[$cardGroup]['paid'] += $cardAmount;
                }
            }
        }

        foreach ($balances as $group => $groupBalance) {
            $cardId   = self::GROUP_CARD_IDS[$group];

            $cardName  = $group . ' : ' . number_format($groupBalance['total'], 2);
            $cardName .= ' / '  . number_format($groupBalance['total'] - $groupBalance['paid'], 2) . ' EUR';

            $this->trelloClient->api('card')->setName($cardId, $cardName);
        }
    }

    private function canProcess(string $cardName) : bool
    {
        return strpos($cardName, ':') === false;
    }

    private function parseAmountFromCardName(string $cardName) : float
    {
        list($amount) = explode(' ', $cardName);

        return (float)str_replace(',', '', $amount);
    }

    private function getCardLabels(array $cardLabels) : array
    {
        $labels = [];
        foreach ($cardLabels as $label) {
            if (array_key_exists($label, self::LABELS)) {
                $labels[] = self::LABELS[$label];
            }
        }

        return $labels;
    }
}
