<?php
declare(strict_types=1);

namespace PTX\Trello\Service;

class Archiver
{
    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient)
    {
        $this->trelloClient = $trelloClient;
    }

    public function run(string $cardListId) : void
    {
        $cards = $this->trelloClient->api('list')->cards()->filter($cardListId, 'open');
        foreach ($cards as $card) {
            $this->tryToArchiveCard($card);
        }
    }

    private function tryToArchiveCard(array $card) : void
    {
        if ($card['dueComplete'] !== true) {
            return;
        }

        $lastActivity = new \DatetimeImmutable($card['due']);
        $difference = $lastActivity->diff(new \DatetimeImmutable(), true);
        $dayDifference = (int) $difference->format('%a');
        if ($dayDifference < 14) {
            return;
        }

        $this->trelloClient->api('card')->setClosed($card['id'], true);
    }
}
