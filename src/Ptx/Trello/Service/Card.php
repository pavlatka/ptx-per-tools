<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

class Card
{
    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient, string $doneListId)
    {
        $this->doneListId   = $doneListId;
        $this->trelloClient = $trelloClient;
    }

    public function manageCard(array $payload)
    {
        if (!$this->canProcess($payload)) {
            return true;
        }

        $this->updateCard($payload);
    }

    private function updateCard(array $payload)
    {
        $boardId = $payload['action']['data']['board']['id'];
        if ($boardId !== '5abce3eca7988228b5677176') {
            return;
        }

        $cardId = $payload['action']['data']['card']['id'];
        $card   = $this->trelloClient->api('card')->show($cardId);

        $this->moveToDone($card);
        $this->replaceDateAbbrv($card);
        $this->fixDueDateForCard($card);
    }

    private function moveToDone(array $card)
    {
        $cardListId = $card['idList'];
        if ($cardListId === $this->doneListId) {
            return;
        }

        if ($card['dueComplete'] === true) {
            $this->trelloClient->api('card')->setList($card['id'], $this->doneListId);
        }
    }

    private function replaceDateAbbrv(array &$card)
    {
        $cardName = $card['name'];
        $abbrvs = [
            'TD ' => date('d/m'),
            'TM ' => date('d/m', strtotime('+1 day')),
            'MO ' => date('d/m', strtotime('next Monday')),
            'TU ' => date('d/m', strtotime('next Tuesday')),
            'WE ' => date('d/m', strtotime('next Wednesday')),
            'TH ' => date('d/m', strtotime('next Thursday')),
            'FR ' => date('d/m', strtotime('next Friday')),
            'SA ' => date('d/m', strtotime('next Saturday')),
            'SU ' => date('d/m', strtotime('next Sunday')),
        ];

        $replaced = false;
        foreach ($abbrvs as $key => $replace) {
            if (strpos($cardName, $key) !== false) {
                $cardName = str_replace($key, $replace . ' ', $cardName);
                $replaced = true;
            }
        }

        list ($date, $rest) = explode('-', $cardName);
        if (!empty($rest)) {
            list ($day, $time) = explode(' ', $date);
            if (!empty($time) && strpos($time, ':') === false) {
                $newDate  = $day . ' ' . $time . ':00 ';
                $cardName = str_replace($date, $newDate, $cardName);

                $replaced = true;
            }
        }

        if ($replaced) {
            $this->trelloClient->api('card')->setName($card['id'], $cardName);

            $card['name'] = $cardName;
        }
    }

    private function fixDueDateForCard(array $card)
    {
        if ($card['dueComplete'] === true) {
            return;
        }

        $correctDueDate = $this->parseDueDateFromCardName($card['name']);
        if ($correctDueDate === '') {
            return;
        }

        $correctDueDate = date('Y-m-d H:i', strtotime($correctDueDate . '- 1 hours'));

        list($date) = explode(' ', $correctDueDate);
        if ($date < date('Y-m-d', strtotime('-2weeks'))) {
            $correctDueDate = date('Y-m-d H:i', strtotime($correctDueDate . '+1 year'));
        }

        if ($card['due'] === null) {
            $this->trelloClient->api('card')->setDueDate($card['id'], new \DateTime($correctDueDate));
        } elseif (date('Y-m-d H:i', strtotime($card['due'])) !== $correctDueDate) {
            $this->trelloClient->api('card')->setDueDate($card['id'], new \DateTime($correctDueDate));
        }
    }

    private function parseDueDateFromCardName(string $cardName) : string
    {
        if (preg_match('/^\d+\/\d+ \d+:\d+ - /', $cardName)) {
            list($date, $time) = explode(' ', substr($cardName, 0, 11));
            list($day,$month) = explode('/', $date);

            return date('Y') . '-' . $month . '-' . $day . ' ' . $time;
        }

        if (preg_match('/^\d+\/\d+ - /', $cardName)) {
            list($day,$month) = explode('/', substr($cardName, 0, 5));

            return date('Y') . '-' . $month . '-' . $day . ' 08:00';
        }

        return '';
    }

    private function canProcess(array $payload) : bool
    {
        $action = $this->parseActionFromPayload($payload);

        return in_array($action, ['createCard', 'updateCard'], true);
    }

    private function parseActionFromPayload(array $payload) : string
    {
        return $payload['action']['type'] ?? '';
    }
}
