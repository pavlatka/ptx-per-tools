<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

class Finance
{
    const LABELS = [
        '5c3eed1ca3a82f48723fc5aa' => 'PAYMENT',
        '5c42cbfbb8c77d33efa0c44c' => 'PAYMENT_SPECIAL',
        '5c3eeda77c329d3db0dfd377' => 'PAID',
        '5c3eed1ca3a82f48723fc5a8' => 'INCOME',
        '5c42ca27b366293c498c4042' => 'INCOME_SPECIAL'
    ];

    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient)
    {
        $this->trelloClient = $trelloClient;
    }

    public function manageCard(array $payload)
    {
        if (!$this->canProcess($payload)) {
            return true;
        }

        $this->updateBalance($payload);
    }

    private function updateBalance(array $payload)
    {
        $listId = $this->getListId($payload);
        $cards  = $this->getCardsForCardList($listId);

        $balance     = 0;
        $payment     = 0;
        $onAccount   = 0;
        $toBePaid    = 0;
        $balanceId   = null;
        $onAccountId = null;
        foreach ($cards as $card) {
            if (strpos($card['name'], 'BAL') === 0) {
                $balanceId = $card['id'];
            } elseif (strpos($card['name'], 'OAN') === 0) {
                $onAccountId = $card['id'];
            } else {
                $amount = $this->parseAmountFromCardName($card['name']);
                $cardLabels = $this->getCardLabels($card['idLabels']);

                if (in_array('PAYMENT', $cardLabels) || in_array('PAYMENT_SPECIAL', $cardLabels)) {
                    $payment += $amount;
                    $balance -= $amount;

                    if (in_array('PAID', $cardLabels)) {
                      $onAccount -= $amount;
                    } else {
                      $toBePaid += $amount;
                    }
                }

                if (in_array('INCOME', $cardLabels) || in_array('INCOME_SPECIAL', $cardLabels)) {
                    $balance   += $amount;
                    $onAccount += $amount;
                }
            }
        }

        if ($balanceId !== null) {
            $cardName = 'BAL : ' . number_format($payment, 2) . ' / '  . number_format($balance, 2) . ' EUR';
            $this->trelloClient->api('card')->setName($balanceId, $cardName);
        }

        if ($onAccountId !== null) {
            $cardName = 'OAN : ' . number_format($onAccount, 2) . ' / '  . number_format($toBePaid, 2) . ' EUR';
            $this->trelloClient->api('card')->setName($onAccountId, $cardName);
        }
    }

    private function parseAmountFromCardName(string $cardName) : float
    {
        list($amount) = explode(' ', $cardName);

        return (float)str_replace(',', '', $amount);
    }

    private function getCardLabels(array $cardLabels) : array
    {
        $labels = [];
        foreach ($cardLabels as $label) {
            if (array_key_exists($label, self::LABELS)) {
                $labels[] = self::LABELS[$label];
            }
        }

        return $labels;
    }

    private function getListId(array $payload) : string
    {
        $cardId = $payload['action']['data']['card']['id'];
        $card   = $this->trelloClient->api('card')->show($cardId);

        return $card['idList'];
    }

    private function canProcess(array $payload) : bool
    {
        $boardId = $payload['action']['data']['board']['id'];

        return $boardId === '5c3eed1cda3a2b56d26e3e71';
    }

    private function getCardsForCardList(string $listId) : array
    {
        return $this->trelloClient->api('list')->cards()->filter($listId, 'open');
    }
}
