<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

use Ptx\Trello\DAO\PlannerDAO;

class Planner
{
    private $settings;
    private $trelloClient;
    private $plannerDao;

    public function __construct(
        \Trello\Client $trelloClient,
        PlannerDAO $plannerDao,
        array $settings
    ) {
        $this->settings     = $settings;
        $this->plannerDao   = $plannerDao;
        $this->trelloClient = $trelloClient;
    }

    public function run()
    {
        $plans = $this->getData();
        foreach ($plans as $plan) {
            $this->handlePlan($plan);
        }
    }

    private function handlePlan(array $plan) : void
    {
        if (!$this->shouldBeHandledNow($plan)) {
            return;
        }

        $this->createCard($plan);
    }

    private function createCard(array $plan)
    {
        $nextRun = $this->getNewRunDate($plan);

        $name = date('d/m', strtotime($nextRun));
        if (!empty($plan['time'])) {
            $name .= ' ' . $plan['time'];
        }

        $card = [
            'name'     => $name . ' - ' . $plan['title'],
            'idList'   => $this->settings['column'],
            'idLabels' => $this->getLabelIds($plan['labels']),
        ];

        return $this->trelloClient->api('card')->create($card);
    }

    private function getLabelIds(array $labels) : string
    {
        $labelIds = [];
        foreach ($labels as $label) {
            if (array_key_exists($label, $this->settings['labels'])) {
                $labelIds[] = $this->settings['labels'][$label];
            }
        }

        return !empty($labelIds) ? implode(',', $labelIds) : '';
    }

    private function shouldBeHandledNow(array $plan) : bool
    {
        $runDate = $this->getNewRunDate($plan);

        return $runDate === date('Y-m-d', strtotime('+5 days'));
    }

    private function getNewRunDate(array $plan)
    {
        $startDate = $plan['start'];
        $period    = $plan['repeat'];
        $today     = date('Y-m-d');

        $nextRun = $startDate;

        while ($nextRun < $today) {
            $nextRun = date('Y-m-d', strtotime($nextRun . ' ' . $period));
        }

        return $nextRun;
    }

    private function getData() : array
    {
        return $this->plannerDao->getData();
    }
}
