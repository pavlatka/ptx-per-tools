<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

use Google_Service_Calendar;
use Google_Service_Calendar_Event;

class GoogleCalendar
{
    private $calendarId;
    private $calendarService;
    private $trelloClient;

    public function __construct(
        string $calendarId,
        \Trello\Client $trelloClient,
        Google_Service_Calendar $calendarService
    ) {
        $this->calendarId      = $calendarId;
        $this->trelloClient    = $trelloClient;
        $this->calendarService = $calendarService;
    }

    public function setInCalendar(array $payload)
    {
        $card  = $this->getCard($payload['action']['data']['card']['id']);
        if (empty($card['due'])) {
            return;
        }

        $event = $this->createCalendarEvent($card);

        $eventId = $this->getEventId($card['id']);
        if ($eventId === null) {
            $this->calendarService->events->insert($this->calendarId, $event);
        } else {
            $this->calendarService->events->update($this->calendarId, $eventId, $event);
        }
    }

    private function getCard(string $cardId) : array
    {
        return $this->trelloClient->api('card')->show($cardId);
    }

    private function createCalendarEvent(array $card) : Google_Service_Calendar_Event
    {
        return new Google_Service_Calendar_Event([
            'summary'     => $card['name'],
            'description' => $this->getDescription($card),
            'start'       => [
                'dateTime' => date('Y-m-d\TH:i:s', strtotime($card['due'])),
                'timeZone' => 'Europe/London'
            ],
            'end'       => [
                'dateTime' => date('Y-m-d\TH:i:s', strtotime($card['due'] . '+25 min')),
                'timeZone' => 'Europe/London'
            ],
        ]);
    }

    private function getDescription(array $card) : string
    {
        $desc = $card['desc'];

        $desc .= '<br /> - URL: ' . $card['url'];
        $desc .= '<br /> - ' . $this->getCardIdKey($card['id']);

        return $desc;
    }

    private function getEventId(string $cardId) : ?string
    {
        $opParams = [
            'maxResults'   => 1,
            'singleEvents' => true,
            'orderBy'      => 'startTime',
            'q'            => $this->getCardIdKey($cardId)
        ];

        $result = $this->calendarService->events->listEvents($this->calendarId, $opParams);

        $events = $result->getItems();
        foreach ($events as $event) {
            return $event->getId();
        }

        return null;
    }

    private function getCardIdKey(string $cardId) : string
    {
        return 'TID: ' . $cardId;
    }
}
