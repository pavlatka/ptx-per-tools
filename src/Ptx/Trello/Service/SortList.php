<?php
declare(strict_types = 1);

namespace Ptx\Trello\Service;

class SortList
{
    const ACTION_TO_SORT = [
        'createCard'
    ];

    const BOARD_ID_TO_SORT = [
        '5abce3eca7988228b5677176'
    ];

    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient)
    {
        $this->trelloClient = $trelloClient;
    }

    public function sortList(array $payload) : void
    {
        if (!$this->canStartSort($payload)) {
            return;
        }

        $listId = $this->getListId($payload);

        $cards       = $this->getCardsForCardList($listId);
        $sortedCards = $this->sortCards($cards);
        foreach ($sortedCards as $position => $cardId) {
            $this->trelloClient->api('card')->setPosition($cardId, $position);
        }
    }

    private function canStartSort(array $payload) : bool
    {
        $actionType = $payload['action']['type'] ?? 'undefined';
        if (!in_array($actionType, self::ACTION_TO_SORT, true)) {
            return false;
        };

        $boardId = $payload['action']['data']['board']['id'] ?? 'undefined';
        if (!in_array($boardId, self::BOARD_ID_TO_SORT, true)) {
            return false;
        };

        return true;
    }

    private function getListId(array $payload) : string
    {
        $cardId = $payload['action']['data']['card']['id'];
        $card   = $this->trelloClient->api('card')->show($cardId);

        return $card['idList'];
    }

    private function sortCards(array $cards) : array
    {
        $structured = [];
        foreach ($cards as $card) {
            $cardId  = $card['id'];
            $dueDate = empty($card['due']) ? $this->getFakeDate() : date('Y-m-d H:i', strtotime($card['due']));

            $structured[$dueDate][] = $card['id'];
        }

        ksort($structured);

        $sorted = [];
        foreach ($structured as $cards) {
            foreach ($cards as $cardId) {
                $sorted[] = $cardId;
            }
        }

        return $sorted;
    }

    private function getFakeDate() : string {
        static $counter = 1;

        return date('Y-m-d H:i', strtotime('1982-09-26 08:00 +' . $counter++ . ' day'));
    }

    private function getCardsForCardList(string $listId) : array
    {
        return $this->trelloClient->api('list')->cards()->filter($listId, 'open');
    }
}
