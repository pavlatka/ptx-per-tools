<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

class PrepareDay
{
    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient)
    {
        $this->trelloClient = $trelloClient;
    }

    public function prepareDay(array $cardLists, array $cards)
    {
        $this->moveCardsToProperCardList($cardLists);
        $this->fixLeadingCards($cards);
        $this->reorderCardsInCardLists($cardLists);
    }

    private function reorderCardsInCardLists(array $cardLists)
    {
        $this->reorderCardsInCardList($cardLists['2days'], false);
        $this->reorderCardsInCardList($cardLists['5days'], false);
        $this->reorderCardsInCardList($cardLists['14days'], false);
        $this->reorderCardsInCardList($cardLists['planning'], true);
    }

    private function reorderCardsInCardList(string $cardListId, bool $planning)
    {
        $cards = $this->getCardsForCardList($cardListId);

        $structure     = [];
        $noDateCounter = 1;
        foreach ($cards as $key => $card) {
            if ($key === 0) {
                $structure['1970-01-01'][] = $card;
            } else if (!preg_match('/^\d+\/\d+ /', $card['name'])) {
                $structure[date('Y-m-d', strtotime('1970-01-01 + ' . $noDateCounter++ . 'days'))][] = $card;
            } else {
                $date = $this->parseDateFromCardName($card['name']);
                if ($planning && $date < date('Y-m-d')) {
                    $date = date('Y-m-d', strtotime($date . ' + 1 year'));
                }

                $structure[$date][] = $card;
            }
        }

        ksort($structure);

        $position = 1;
        foreach ($structure as $group) {
            foreach ($group as $card) {
                $newPosition = ++$position;
                if ($card['pos'] !== $newPosition) {
                    $this->setCartPosition($card['id'], $newPosition);
                }
            }
        }
    }

    private function moveCardsToProperCardList(array $cardLists)
    {
        $this->moveCardListCardsToProperCardList($cardLists['5days'], $cardLists);
        $this->moveCardListCardsToProperCardList($cardLists['14days'], $cardLists);
        $this->moveCardListCardsToProperCardList($cardLists['planning'], $cardLists);
    }

    private function moveCardListCardsToProperCardList(string $cardListId, array $cardLists)
    {
        $cards = $this->getCardsForCardList($cardListId);

        foreach ($cards as $card) {
            $cardName = $card['name'];
            if (!preg_match('/^\d+\/\d+ /', $cardName)) {
                continue;
            }

            $date       = $this->parseDateFromCardName($cardName);
            $periodCode = $this->findPeriodCodeForDate($date);
            if ($periodCode === 'planning') {
                continue;
            }

            $cardListId = $cardLists[$periodCode] ?? $cardLists['14days'];
            if ($card['idList'] !== $cardListId) {
                $this->moveCardToList($card['id'], $cardListId);
            }
        }
    }

    private function findPeriodCodeForDate(string $date) : string
    {
        $periodBorders = $this->getPeriodBorders();

        foreach ($periodBorders as $periodCode => $borders) {
            if ($date >= $borders['start'] && $date <= $borders['end']) {
                return $periodCode;
            }
        }

        return 'planning';
    }

    private function parseDateFromCardName(string $cardName) : string
    {
        if (!preg_match('/^\d+\/\d+ /', $cardName)) {
            throw new \RuntimeException('Cannot parse date: ' . $cardName);
        }

        $date = substr($cardName, 0, 5);
        list($day, $month) = explode('/', $date);

        return date('Y') . '-' . $month . '-' . $day;
    }


    private function fixLeadingCards(array $cards)
    {
        $this->fixCurrentPeriodLeadingCard($cards['2days'], true);
        $this->fixNextPeriodLeadingCard($cards['5days']);
        $this->fixPlanningPeriodLeadingCard($cards['14days']);
    }

    private function fixPlanningPeriodLeadingCard(string $cardId)
    {
        $borders        = $this->getPeriodBorders();
        $currentBorders = $borders['14days'];

        $cardName = date('D d/m', strtotime($currentBorders['start'])) . ' - ';
        $cardName .= date('D d/m', strtotime($currentBorders['end']));

        $this->setCartName($cardId, $cardName);
        $this->setCartPosition($cardId, 1);
    }

    private function fixNextPeriodLeadingCard(string $cardId)
    {
        $borders        = $this->getPeriodBorders();
        $currentBorders = $borders['5days'];

        $cardName = date('D d/m', strtotime($currentBorders['start'])) . ' - ';
        $cardName .= date('D d/m', strtotime($currentBorders['end']));

        $this->setCartName($cardId, $cardName);
        $this->setCartPosition($cardId, 1);
    }

    private function fixCurrentPeriodLeadingCard(string $cardId, bool $addWeek)
    {
        $borders        = $this->getPeriodBorders();
        $currentBorders = $borders['2days'];

        $cardName = date('D d/m', strtotime($currentBorders['start'])) . ' - ';
        $cardName .= date('D d/m', strtotime($currentBorders['end']));

        if ($addWeek) {
            $cardName .= ', Week: ' . (date('W') % 2 === 0 ? 'WA' : 'WB');
        }

        $this->setCartName($cardId, $cardName);
        $this->setCartPosition($cardId, 1);
    }

    private function setCartName(string $cardId, string $cardName)
    {
        $this->trelloClient->api('card')->setName($cardId, $cardName);
    }

    private function setCartPosition(string $cardId, int $position)
    {
        $this->trelloClient->api('card')->setPosition($cardId, $position);
    }

    private function getPeriodBorders() : array
    {
        return [
            '2days' => [
                'start' => date('Y-m-d'),
                'end'   => date('Y-m-d', strtotime('+1 days'))
            ],
            '5days' => [
                'start' => date('Y-m-d', strtotime('+2 days')),
                'end'   => date('Y-m-d', strtotime('+6 days'))
            ],
            '14days' => [
                'start' => date('Y-m-d', strtotime('+7 days')),
                'end'   => date('Y-m-d', strtotime('+14 days'))
            ],
        ];
    }

    private function getCardsForCardList(string $cardListId) : array
    {
        return $this->trelloClient->api('list')->cards()->filter($cardListId, 'open');
    }

    private function moveCardToList(string $cardId, string $listId)
    {
        $this->trelloClient->api('card')->setList($cardId, $listId);
    }
}
