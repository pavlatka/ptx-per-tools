<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

class Settings
{
    private $trelloClient;

    public function __construct(\Trello\Client $trelloClient)
    {
        $this->trelloClient = $trelloClient;
    }

    public function getSettings()
    {
        $data = [
            'lists' => []
        ];

        $boardId   = 'rPqkHSAt';
        $cardLists = $this->trelloClient->api('board')->lists()->all($boardId);
        foreach ($cardLists as $cardList) {
            $data['lists'][$cardList['id']] = $cardList['name'];
        }

        return $data;
    }
}
