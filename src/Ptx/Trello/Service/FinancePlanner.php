<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

use Ptx\Trello\DAO\FinancePlannerDAO;

class FinancePlanner
{
    private const LABELS = [
        '5c3eed1ca3a82f48723fc5aa' => 'PAYMENT',
        '5c3eeda77c329d3db0dfd377' => 'PAID',
        '5c3eed1ca3a82f48723fc5a8' => 'INCOME',
        '5c3efebc5aa29f85aeaaf2e4' => 'HOUSE',
        '5c3eff29d3199665a254aaf3' => 'CAR',
        '5c41b63a22d7754383c6595e' => 'BVG',
        '5c41b6647c7ef33de1ac589c' => 'POCKET',
        '5c3efeca3f375256e76b6e98' => 'CLUB',
        '5c3eff004a269c85bfcedeee' => 'OTHER',
        '5c3efee76d0e4c5a656f85bf' => 'SCHOOL',
    ];

    private $boardId;
    private $trelloClient;
    private $plannerDao;

    public function __construct(
        \Trello\Client $trelloClient,
        FinancePlannerDAO $plannerDao,
        string $boardId
    ) {
        $this->boardId      = $boardId;
        $this->plannerDao   = $plannerDao;
        $this->trelloClient = $trelloClient;
    }

    public function plan(int $year, int $month)
    {
        $listId    = $this->getListId($year, $month);
        $listCards = $this->trelloClient->api('list')->cards()->filter($listId, 'open');

        $this->createBalanceCard($listId, $listCards);
        $this->createCards($listId, $listCards);
    }

    private function createCards(string $listId, array $listCards) : void
    {
        $data = $this->plannerDao->getData();
        usort($data, function ($a, $b) {
            if ($a['amount'] === $b['amount']) {
                return 0;
            }

            return $a['amount'] < $b['amount'];
        });
        foreach ($data as $card) {
            $this->createCard($card, $listId, $listCards);
        }
    }

    private function createCard(array $card, string $listId, array $listCards) : void
    {
        $cardName = number_format($card['amount'], 2) . ' EUR - ' . $card['reason'];
        foreach ($listCards as $listCard) {
            if ($listCard['name'] === $cardName) {
                return;
            }
        }

        $cardData = [
            'idList'   => $listId,
            'name'     => $cardName,
            'idLabels' => $this->getLabelsIds($card['labels']),
            'pos'      => 'bottom'
        ];

        $this->trelloClient->api('card')->create($cardData);
    }

    private function getLabelsIds(array $labels) : string
    {
        $labelIds = [];
        foreach ($labels as $label) {
            $key = array_search($label, self::LABELS);
            if ($key !== false) {
                $labelIds[] = $key;
            }
        }

        return implode(',', $labelIds);
    }

    private function createBalanceCard(string $listId, array $listCards) : void
    {
        foreach ($listCards as $listCard) {
            if (strpos($listCard['name'], 'BALANCE :') !== false) {
                return;
            }
        }

        $card = [
            'name'     => 'BALANCE : 0.00 EUR',
            'idList'   => $listId
        ];

        $this->trelloClient->api('card')->create($card);
    }

    private function createList(int $year, int $month) : string
    {
        $list = [
            'name'    => $this->createListName($year, $month),
            'idBoard' => $this->boardId
        ];

        return $this->trelloClient->api('lists')->create($list)['id'];
    }

    private function createListName(int $year, int $month) : string
    {
        return $year . '/' . str_pad((string)$month, 2, '0', STR_PAD_LEFT);
    }

    private function getListId(int $year, int $month) : string
    {
        $listName = $this->createListName($year, $month);

        $lists = $this->trelloClient->api('board')->lists()->filter($this->boardId, 'open');
        foreach ($lists as $list) {
            if ($list['name'] === $listName) {
                return $list['id'];
            }
        }

        return $this->createList($year, $month);
    }
}
