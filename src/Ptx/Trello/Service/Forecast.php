<?php
declare(strict_types=1);

namespace Ptx\Trello\Service;

use Ptx\Trello\DAO\ForecastDAO;

class Forecast
{
    const LOCATION = 'Germany/Berlin/Berlin_Pankow';
    const CARD_ID  = '5ba084b42f235682a73da825';

    private $trelloClient;

    public function __construct(
        \Trello\Client $trelloClient,
        ForecastDAO $forecastDao
    ) {
        $this->forecastDao  = $forecastDao;
        $this->trelloClient = $trelloClient;
    }

    public function run()
    {
        $message = $this->getForecastMessage($this->getForecast());

        $this->setCardName($message);
    }

    private function setCardName(string $cardName)
    {
        $this->trelloClient->api('card')->setName(self::CARD_ID, $cardName);
    }

    private function getForecastMessage(array $forecast) : string
    {
        $msg  = 'Temp: ' . $this->getTemperature($forecast) . ' °C';
        $msg .= ', Wind: ' . $this->getWind($forecast) . ' kph';
        $msg .= ', Rain: ' . $this->getPrecipitation($forecast) . ' %';

        return $msg;
    }

    private function getTemperature(array $forecast) : int
    {
        if (in_array(date('n'), [10, 11, 1, 2, 3])) {
            return $this->getTemperatureWinter($forecast);
        }

        return $this->getTemperatureSummer($forecast);
    }

    private function getTemperatureWinter(array $forecast) : int
    {
        $temp = 9999;

        foreach ($forecast as $row) {
            if ($row['temp'] < $temp) {
                $temp = $row['temp'];
            }
        }

        return $temp;
    }

    private function getTemperatureSummer(array $forecast) : int
    {
        $temp = 0;

        foreach ($forecast as $row) {
            if ($row['temp'] > $temp) {
                $temp = $row['temp'];
            }
        }

        return $temp;
    }

    private function getWind(array $forecast) : float
    {
        $wind = 0;

        foreach ($forecast as $row) {
            if ($row['wind_mps'] > $wind) {
                $wind = $row['wind_mps'];
            }
        }

        return ceil($wind * 3.6);
    }

    private function getPrecipitation(array $forecast) : int
    {
        $precipitation = 0;

        foreach ($forecast as $row) {
            if ($row['precipitation'] > $precipitation) {
                $precipitation = $row['precipitation'];
            }
        }

        return $precipitation;
    }

    private function getForecast() : array
    {
        return $this->forecastDao->getForecast(self::LOCATION);
    }
}
