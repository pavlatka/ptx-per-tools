<?php

require_once __DIR__ . '/../vendor/autoload.php';

$config = [
    'settings' => [
        'displayErrorDetails' => true
    ]
];
$app = new \Slim\App($config);

require_once __DIR__ . '/routes.php';
require_once __DIR__ . '/dependencies.php';

$app->run();
