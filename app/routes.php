<?php

$app->get('/', function () {
    return 'Silence is golden!';
});

$files = glob(__DIR__ . '/../src/*/*/Config/routes.php');
foreach ($files as $file) {
    require_once $file;
}
